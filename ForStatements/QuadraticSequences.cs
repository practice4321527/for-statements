﻿namespace ForStatements
{
    public static class QuadraticSequences
    {
        public static uint CountQuadraticSequenceTerms(long a, long b, long c, long maxTerm)
        {
            // TODO Task 6. Implement the method that returns the number of the quadratic sequence terms that are less than or equals to the maxTerm.
            throw new NotImplementedException();
        }

        public static ulong GetQuadraticSequenceTermsProduct1(uint count)
        {
            // TODO Task 7. Implement the method that returns the product of the first count quadratic sequence terms.
            throw new NotImplementedException();
        }

        public static ulong GetQuadraticSequenceProduct2(long a, long b, long c, long startN, long count)
        {
            // TODO Task 8. Implement the method that returns the product of count quadratic sequence terms starting with the startN term.
            throw new NotImplementedException();
        }
    }
}
